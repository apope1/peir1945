package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class IntegrationTestTopDown {

    String enuntIntrebare, raspuns1, raspuns2, raspuns3, raspunsulCorect, domeniul, errorMessage;
    AppController appController;

    @Before
    public void setUp() throws Exception {
        appController = new AppController();
    }

    @Test
    public void unitTestA() {
        enuntIntrebare = "Cand a inceput al doilea razboi mondial?";
        raspuns1 = "1) 1234";
        raspuns2 = "2) 1939";
        raspuns3 = "3) 1920";
        raspunsulCorect = "2";
        domeniul = "Istorie";
        errorMessage = "";
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare, raspuns1, raspuns2, raspuns3, raspunsulCorect, domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage, e.getMessage());
            return;
        }
        Assert.assertEquals(1, intrebari.size());
    }

    @Test
    public void integrationTestTopDownAB() {
        enuntIntrebare = "Cand a inceput al doilea razboi mondial?";
        raspuns1 = "1) 1234";
        raspuns2 = "2) 1939";
        raspuns3 = "3) 1920";
        raspunsulCorect = "2";
        domeniul = "Istorie";
        errorMessage = "";
        appController = new AppController();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare, raspuns1, raspuns2, raspuns3, raspunsulCorect, domeniul));
            Assert.assertEquals(1, intrebari.size());
            Intrebare intrebare1 = new Intrebare(enuntIntrebare, raspuns1, raspuns2, raspuns3, raspunsulCorect, domeniul);
            Intrebare intrebare2 = new Intrebare("Suprafata Romaniei?", "1) 1234", "2) 1939", "3) 1920", "2", "Geografie");
            Intrebare intrebare3 = new Intrebare("Cate grupe de sange sunt?", "1) 1234", "2) 1939", "3) 1920", "2", "Medicina");
            Intrebare intrebare4 = new Intrebare("Cand a aparut WWW?", "1) 1234", "2) 1939", "3) 1920", "2", "Informatica");
            Intrebare intrebare5 = new Intrebare("Teorema lui Pitagora?", "1) 1234", "2) 1939", "3) 1920", "2", "Matematica");
            appController.addNewIntrebare(intrebare1);
            appController.addNewIntrebare(intrebare2);
            appController.addNewIntrebare(intrebare3);
            appController.addNewIntrebare(intrebare4);
            appController.addNewIntrebare(intrebare5);
            evaluator.model.Test test = appController.createNewTest();
            Assert.assertEquals(5, test.getIntrebari().size());
        } catch (InputValidationFailedException | DuplicateIntrebareException | NotAbleToCreateTestException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void integrationTestBigBang() {
        enuntIntrebare = "Cand a inceput al doilea razboi mondial?";
        raspuns1 = "1) 1234";
        raspuns2 = "2) 1939";
        raspuns3 = "3) 1920";
        raspunsulCorect = "2";
        domeniul = "Istorie";
        errorMessage = "";
        appController = new AppController();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare, raspuns1, raspuns2, raspuns3, raspunsulCorect, domeniul));
            Assert.assertEquals(1, intrebari.size());
            Intrebare intrebare1 = new Intrebare(enuntIntrebare, raspuns1, raspuns2, raspuns3, raspunsulCorect, domeniul);
            Intrebare intrebare2 = new Intrebare("Suprafata Romaniei?", "1) 1234", "2) 1939", "3) 1920", "2", "Geografie");
            Intrebare intrebare3 = new Intrebare("Cate grupe de sange sunt?", "1) 1234", "2) 1939", "3) 1920", "2", "Medicina");
            Intrebare intrebare4 = new Intrebare("Cand a aparut WWW?", "1) 1234", "2) 1939", "3) 1920", "2", "Informatica");
            Intrebare intrebare5 = new Intrebare("Teorema lui Pitagora?", "1) 1234", "2) 1939", "3) 1920", "2", "Matematica");
            appController.addNewIntrebare(intrebare1);
            appController.addNewIntrebare(intrebare2);
            appController.addNewIntrebare(intrebare3);
            appController.addNewIntrebare(intrebare4);
            appController.addNewIntrebare(intrebare5);
            evaluator.model.Test test = appController.createNewTest();
            Assert.assertEquals(5, test.getIntrebari().size());
            Statistica statistica = appController.getStatistica();
            Assert.assertEquals(5, statistica.getIntrebariDomenii().size());
        } catch (InputValidationFailedException | DuplicateIntrebareException | NotAbleToCreateTestException | NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
        }
    }
}
