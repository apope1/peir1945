package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AppControllerTest {

    AppController appController;

    @Before
    public void setUp() throws Exception {
        appController = new AppController();
    }

    public void setUpReq02_TC01() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare1 = new Intrebare("Cand a inceput WWII?", "1) 1234", "2) 1939", "3) 1920", "2", "Istorie");
        Intrebare intrebare2 = new Intrebare("Suprafata Romaniei?", "1) 1234", "2) 1939", "3) 1920", "2", "Geografie");
        Intrebare intrebare3 = new Intrebare("Cate grupe de sange sunt?", "1) 1234", "2) 1939", "3) 1920", "2", "Medicina");
        Intrebare intrebare4 = new Intrebare("Cand a aparut WWW?", "1) 1234", "2) 1939", "3) 1920", "2", "Informatica");
        appController.addNewIntrebare(intrebare1);
        appController.addNewIntrebare(intrebare2);
        appController.addNewIntrebare(intrebare3);
        appController.addNewIntrebare(intrebare4);
    }

    public void setUpReq02_TC02() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare1 = new Intrebare("Cand a inceput WWII?", "1) 1234", "2) 1939", "3) 1920", "2", "Istorie");
        Intrebare intrebare2 = new Intrebare("Suprafata Romaniei?", "1) 1234", "2) 1939", "3) 1920", "2", "Geografie");
        Intrebare intrebare3 = new Intrebare("Cate grupe de sange sunt?", "1) 1234", "2) 1939", "3) 1920", "2", "Medicina");
        Intrebare intrebare4 = new Intrebare("Cand a aparut WWW?", "1) 1234", "2) 1939", "3) 1920", "2", "Informatica");
        Intrebare intrebare5 = new Intrebare("Cand s-a terminat WWII?", "1) 1234", "2) 1939", "3) 1920", "2", "Istorie");
        appController.addNewIntrebare(intrebare1);
        appController.addNewIntrebare(intrebare2);
        appController.addNewIntrebare(intrebare3);
        appController.addNewIntrebare(intrebare4);
        appController.addNewIntrebare(intrebare5);
    }

    public void setUpReq02_TC03() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare1 = new Intrebare("Cand a inceput WWII?", "1) 1234", "2) 1939", "3) 1920", "2", "Istorie");
        Intrebare intrebare2 = new Intrebare("Suprafata Romaniei?", "1) 1234", "2) 1939", "3) 1920", "2", "Geografie");
        Intrebare intrebare3 = new Intrebare("Cate grupe de sange sunt?", "1) 1234", "2) 1939", "3) 1920", "2", "Medicina");
        Intrebare intrebare4 = new Intrebare("Cand a aparut WWW?", "1) 1234", "2) 1939", "3) 1920", "2", "Informatica");
        Intrebare intrebare5 = new Intrebare("Teorema lui Pitagora?", "1) 1234", "2) 1939", "3) 1920", "2", "Matematica");
        appController.addNewIntrebare(intrebare1);
        appController.addNewIntrebare(intrebare2);
        appController.addNewIntrebare(intrebare3);
        appController.addNewIntrebare(intrebare4);
        appController.addNewIntrebare(intrebare5);
    }


    @Test
    public void createNewTest_TC01() {
        try {
            setUpReq02_TC01();
            evaluator.model.Test test = appController.createNewTest();
        } catch (InputValidationFailedException | DuplicateIntrebareException | NotAbleToCreateTestException e) {
            Assert.assertEquals("Nu exista suficiente intrebari pentru crearea unui test!(5)", e.getMessage());
        }
    }

    @Test
    public void createNewTest_TC02() {
        try {
            setUpReq02_TC02();
            evaluator.model.Test test = appController.createNewTest();
        } catch (InputValidationFailedException | DuplicateIntrebareException | NotAbleToCreateTestException e) {
            Assert.assertEquals("Nu exista suficiente domenii pentru crearea unui test!(5)", e.getMessage());
        }
    }

    @Test
    public void createNewTest_TC03() {
        try {
            setUpReq02_TC03();
            evaluator.model.Test test = appController.createNewTest();
            Assert.assertEquals(5, test.getIntrebari().size());
        } catch (InputValidationFailedException | DuplicateIntrebareException | NotAbleToCreateTestException e) {
            Assert.assertEquals("Nu exista suficiente intrebari pentru crearea unui test!(5)", e.getMessage());
        }
    }

    @Test
    public void req03_TC01() {
        try {
            setUpReq02_TC03();
            Statistica statistica = appController.getStatistica();
            Assert.assertEquals(5, statistica.getIntrebariDomenii().size());

        } catch (InputValidationFailedException | DuplicateIntrebareException | NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void req03_TC02() {
        try {
            appController.getStatistica();
        } catch (NotAbleToCreateStatisticsException e) {
            Assert.assertEquals("Repository-ul nu contine nicio intrebare!", e.getMessage());
        }
    }
}