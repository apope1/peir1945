package evaluator.main;

import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class StartAppTest {

    String enuntIntrebare, raspuns1, raspuns2, raspuns3, raspunsulCorect, domeniul, errorMessage;

    @Before
    public void setUp() throws Exception {
    }

    private void setUpTC1_ECP() {
        enuntIntrebare = "Cand a inceput al doilea razboi mondial?";
        raspuns1 = "1) 1918";
        raspuns2 = "2) 1957";
        raspuns3 = "3) 1939";
        raspunsulCorect = "3";
        domeniul = "Istorie";
        errorMessage = "";
    }

    private void setUpTC2_ECP() {
        enuntIntrebare = "intrebare fara semn";
        raspuns1 = "12";
        raspuns2 = "12.3";
        raspuns3 = "1";
        raspunsulCorect = "85.53";
        domeniul = "??";
        errorMessage = "Prima litera din enunt nu e majuscula!";
    }

    private void setUpTC3_ECP() {
        enuntIntrebare = "";
        raspuns1 = "";
        raspuns2 = "";
        raspuns3 = "";
        raspunsulCorect = "";
        domeniul = "";
        errorMessage = "Enuntul este vid!";
    }

    private void setUpTC1_BVA() {
        enuntIntrebare = "";
        raspuns1 = "1) a";
        raspuns2 = "2) b";
        raspuns3 = "3) c";
        raspunsulCorect = "2";
        domeniul = "Intrebari";
        errorMessage = "Enuntul este vid!";
    }

    private void setUpTC2_BVA() {
        enuntIntrebare = "?";
        raspuns1 = "1) a";
        raspuns2 = "2) b";
        raspuns3 = "3) c";
        raspunsulCorect = "2";
        domeniul = "Intrebari";
        errorMessage = "Prima litera din enunt nu e majuscula!";
    }

    private void setUpTC3_BVA() {
        enuntIntrebare = "M?";
        raspuns1 = "1) a";
        raspuns2 = "2) b";
        raspuns3 = "3) c";
        raspunsulCorect = "2";
        domeniul = "Intrebari";
        errorMessage = "Enuntul este vid!";
    }

    private void setUpTC4_BVA() {
        StringBuilder enuntInt = new StringBuilder("M");
        for(int i=0; i<97; i++) {
            enuntInt.append(".");
        }
        enuntInt.append("?");
        enuntIntrebare = enuntInt.toString();
        raspuns1 = "1) a";
        raspuns2 = "2) b";
        raspuns3 = "3) c";
        raspunsulCorect = "2";
        domeniul = "Intrebari";
        errorMessage = "";
    }

    private void setUpTC6_BVA() {
        StringBuilder enuntInt = new StringBuilder("M");
        for(int i=0; i<99; i++) {
            enuntInt.append(".");
        }
        enuntInt.append("?");
        enuntIntrebare = enuntInt.toString();
        raspuns1 = "1) a";
        raspuns2 = "2) b";
        raspuns3 = "3) c";
        raspunsulCorect = "2";
        domeniul = "Intrebari";
        errorMessage = "Lungimea enuntului depaseste 100 de caractere!";
    }

    private void setUpTC5_BVA() {
        StringBuilder enuntInt = new StringBuilder("M");
        for(int i=0; i<98; i++) {
            enuntInt.append(".");
        }
        enuntInt.append("?");
        enuntIntrebare = enuntInt.toString();
        raspuns1 = "1) a";
        raspuns2 = "2) b";
        raspuns3 = "3) c";
        raspunsulCorect = "2";
        domeniul = "Intrebari";
        errorMessage = "Lungimea enuntului depaseste 100 de caractere!";
    }

    private void setUpTC8_BVA() {
        StringBuilder enuntInt = new StringBuilder("M");
        for(int i=0; i<98; i++) {
            enuntInt.append(".");
        }
        enuntInt.append("?");
        enuntIntrebare = enuntInt.toString();
        raspuns1 = "1) a";
        raspuns2 = "2) b";
        raspuns3 = "3) c";
        raspunsulCorect = "5";
        domeniul = "Intrebari";
        errorMessage = "Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}";
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addIntrebareECP1() {
        setUpTC1_ECP();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare,raspuns1,raspuns2,raspuns3,raspunsulCorect,domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage,e.getMessage());
            return;
        }
        Assert.assertEquals(1,intrebari.size());
    }

    @Test
    public void addIntrebareECP2() {
        setUpTC2_ECP();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare,raspuns1,raspuns2,raspuns3,raspunsulCorect,domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage,e.getMessage());
            return;
        }
        Assert.assertEquals(1,intrebari.size());
    }

    @Test
    public void addIntrebareECP3() {
        setUpTC3_ECP();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare,raspuns1,raspuns2,raspuns3,raspunsulCorect,domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage,e.getMessage());
            return;
        }
        Assert.assertEquals(1,intrebari.size());
    }

    @Test
    public void addIntrebareBVA1() {
        setUpTC1_BVA();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare,raspuns1,raspuns2,raspuns3,raspunsulCorect,domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage,e.getMessage());
            return;
        }
        Assert.assertEquals(1,intrebari.size());
    }

    @Test
    public void addIntrebareBVA2() {
        setUpTC2_BVA();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare,raspuns1,raspuns2,raspuns3,raspunsulCorect,domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage,e.getMessage());
            return;
        }
        Assert.assertEquals(1,intrebari.size());
    }

    @Test
    public void addIntrebareBVA3() {
        setUpTC3_BVA();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare,raspuns1,raspuns2,raspuns3,raspunsulCorect,domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage,e.getMessage());
            return;
        }
        Assert.assertEquals(1,intrebari.size());
    }

    @Test
    public void addIntrebareBVA4() {
        setUpTC4_BVA();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare,raspuns1,raspuns2,raspuns3,raspunsulCorect,domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage,e.getMessage());
            return;
        }
        Assert.assertEquals(1,intrebari.size());
    }

    @Test
    public void addIntrebareBVA5() {
        setUpTC5_BVA();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare,raspuns1,raspuns2,raspuns3,raspunsulCorect,domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage,e.getMessage());
            return;
        }
        Assert.assertEquals(1,intrebari.size());
    }

    @Test
    public void addIntrebareBVA6() {
        setUpTC6_BVA();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare,raspuns1,raspuns2,raspuns3,raspunsulCorect,domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage,e.getMessage());
            return;
        }
        Assert.assertEquals(1,intrebari.size());
    }

    @Test
    public void addIntrebareBVA8() {
        setUpTC8_BVA();
        ArrayList<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add(new Intrebare(enuntIntrebare,raspuns1,raspuns2,raspuns3,raspunsulCorect,domeniul));
        } catch (InputValidationFailedException e) {
            Assert.assertEquals(errorMessage,e.getMessage());
            return;
        }
        Assert.assertEquals(1,intrebari.size());
    }
}